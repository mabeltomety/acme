<?php 

// $catList = "<select name='categoryId'>"; //category id? puedo?
// foreach ($categories as $category) {
//     $catList .= "<option value='" . $category["categoryId"]. "'>" . $category["categoryName"]. "</option>";
// }
// $catList .= "</select>";

//Build the categories option list
$catList = '<select name="$categoryId" id="categoryId">';
$catList = "<option>Choose a Category</option>";
foreach ($categories as $category) {
  $catlist = "<option value='$category[categoryId]'";
  if (isset($categoryId){
    if($category['categoryId'] === $categoryId){
      $catlist .= ' selected ';
    }
  }
  $catList .=  ">$category[categoryName]"</option>;
} 
$catList .= '</select'>; 

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="CIT336">
  <meta name="keywords" content="PHP, CIT336, ACME">
  <meta name="author" content="Mabel">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>New Category</title>
  <link rel="stylesheet" type="text/css" href="/acme/css/style.css">
</head>

<body>
  <div id="content">
    <header>
      <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/header.php"); ?>
       </header>

       <nav>
    <?php echo $navList; ?>
    </nav>

    <main>
    
    <?php
            if (isset($message)) {
                echo $message;
            }
            ?>
            <form method="post" action="/acme/products/index.php">
                <h1>Add Product</h1>
                <p>Add a new product below. All fields are requiered!<p>

                    Category<br>
                    <?php echo $catList; ?><br>
                    
                    <!--                    in DB-->
                    Product Name<br>
                    <input type="text" name="invName" id="invName" required <?php if(isset($invName)){echo "value='$invName'";}  ?>><br>
                    <!--                    in DB-->                    
                    Product Description<br>
                    <input type="text" name="invDescription" id="invDescription" required <?php if(isset($invDescription)){echo "value='$invDescription'";}  ?>><br>
                    <!--                    in DB-->
                    Product Image (path to image)<br>
                    <input type="text" name="invImage" id="invImage" required <?php if(isset($invImage)){echo "value='$invImage'";} ?>><br>

                    Product Thumbnail (path to thumbnail)<br>
                    <input type="text" name="invThumbnail" id="invThumbnail" required <?php if(isset($invThumbnail)){echo "value='$invThumbnail'";} ?>><br>

                    <!--                    in DB-->                    
                    Product Price<br>
                    <input type="text" name="invPrice" id="invPrice" required <?php if(isset($invPrice)){echo "value='$invPrice'";} ?>><br>
                    <!--                    in DB-->
                    # in Stock<br>
                    <input type="text" name="invStock" id="invStock" required <?php if(isset($invStock)){echo "value='$invStock'";} ?>><br>

                    Shipping Size ( W x H x L in inches )<br>
                    <input type="text" name="invSize" id="invSize" required <?php if(isset($invSize)){echo "value='$invSize'";} ?>><br>
                    Weight (lbs.)<br>
                    <input type="text" name="invWeight" id="invWeight" required <?php if(isset($invWeight)){echo "value='$invWeight'";} ?>><br>
                    Location (city name)<br>
                    <input type="text" name="invLocation" id="invLocation" required <?php if(isset($invLocation)){echo "value='$invLocation'";} ?>><br>

                    <!--                    in DB-->
                    Vendor Name<br>
                    <input type="text" name="invVendor" id="invVendor" required <?php if(isset($invVendor)){echo "value='$invVendor'";} ?>><br>

                    Primary Material<br>
                    <input type="text" name="invStyle" id="invStyle" required <?php if(isset($invStyle)){echo "value='$invStyle'";} ?>><br><br>

                    <input class="buttons" type="submit" name="submit" value="Add Product">
                    <input type="hidden" name="action" value="newProduct"><br>
            </form>
               
    
    </main>

    <footer>
       <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/footer.php"); ?>
       </footer>
  </div>
</body>

</html>