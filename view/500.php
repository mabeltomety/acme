<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="CIT336">
  <meta name="keywords" content="PHP, CIT336, ACME">
  <meta name="author" content="Mabel">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ERROR PAGE</title>
  <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>

<body>
  <div id="content">
    <header>
      <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/header.php"); ?>
       </header>

    <nav>
    <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/nav.php"); ?>
    <?php echo $navList; ?>
    </nav>

    <main>
      <h1>Server Error
      </h1>
      <p>Sorry, the server experiences an error</p>
    </main>

    <footer>
       <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/footer.php"); ?>
       </footer>
  </div>
</body>

</html>