<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="CIT336">
  <meta name="keywords" content="PHP, CIT336, ACME">
  <meta name="author" content="Mabel">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login ACME</title>
  <link rel="stylesheet" type="text/css" href="/acme/css/style.css">
</head>

<body>
  <div id="content">
    <header>
      <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/header.php"); ?>
       </header>

    <nav>
    <?php //include($_SERVER['DOCUMENT_ROOT']."/Acme/common/nav.php"); ?>
    <?php echo $navList; ?>
    </nav>

    <main>
      <h1>
        LOGIN
      </h1>

      <form>
                <?php
                if (isset($message)) {
                    echo $message;
                }
                ?>
                <form> 

                    <h1>Acme Login</h1>

                    Email Adress<br>
                    <input type="email" name="email" required <?php if(isset($email)){echo "value='$email'";}  ?>><br>
                    <label for="clientPassword">Password:</label> 
<span>Passwords must be at least 8 characters and contain at least 1 number, 1 capital letter and 1 special character</span> 
<input type="password" name="clientPassword" id="clientPassword" required pattern="(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">

                    <input class="buttons" type="submit" value="Login"><br>
                    <input type="hidden" name="action" value="Login">

                    <p class="negrita">Not a member?</p>
                   
                    <a href="/acme/accounts/index.php?action=registerform">Create an Account</a>
                </form>
      
    </main>

    <footer>
       <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/footer.php"); ?>
       </footer>
  </div>
</body>

</html>