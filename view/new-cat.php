<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="CIT336">
  <meta name="keywords" content="PHP, CIT336, ACME">
  <meta name="author" content="Mabel">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>New Category</title>
  <link rel="stylesheet" type="text/css" href="/acme/css/style.css">
</head>

<body>
  <div id="content">
    <header>
      <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/header.php"); ?>
       </header>

       <nav>
    <?php echo $navList; ?>
    </nav>

    <main>
    <?php
            if (isset($message)) {
                echo $message;
            }
            ?>
            <form method="post" action="/acme/products/index.php">
                <h1>Add Category</h1>
                <p>Add a new category of products below:<p>
                    New Category Name<br>
                    <input type="text" name="categoryName" id="categoryName" required <?php if(isset($categoryName)){echo "value='$categoryName'";}  ?>><br><br>

                    <input class="buttons" type="submit" name="submit" value="Add Category">
                    <input type="hidden" name="action" value="newCategory"><br>
            </form>
    </main>

    <footer>
       <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/footer.php"); ?>
       </footer>
  </div>
</body>

</html>