<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="CIT336">
  <meta name="keywords" content="PHP, CIT336, ACME">
  <meta name="author" content="Mabel">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Resgistration ACME</title>
  <link rel="stylesheet" type="text/css" href="/acme/css/style.css">
</head>

<body>
  <div id="content">
    <header>
      <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/header.php"); ?>
       </header>

    <nav>
    <?php //include($_SERVER['DOCUMENT_ROOT']."/Acme/common/nav.php"); ?>
    <?php echo $navList; ?>
    </nav>

    <main>
      <h1>
        REGISTRATION
      </h1>
      <?php
            if (isset($message)) {
                echo $message;
            }
            ?>

            <form method="post" action="/acme/accounts/index.php">
                <h1>Acme Registration</h1>
                <p>All fields are requiered.<p>
                    First Name<br>
                    <input type="text" name="firstname" id="firstname" required <?php if(isset($firstname)){echo "value='$firstname'";}  ?>><br>

                    Last Name<br>
                    <input type="text" name="lastname" id="lasttname" required <?php if(isset($lastname)){echo "value='$lastname'";}  ?>><br>

                    Email Adress<br>
                    <input type="email" name="email" id="email" required <?php if(isset($email)){echo "value='$email'";}  ?>><br>

                    <label for="clientPassword">Password:</label> 
<span>Passwords must be at least 8 characters and contain at least 1 number, 1 capital letter and 1 special character</span> 
<input type="password" name="clientPassword" id="clientPassword" required pattern="(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">

                    <input class="buttons" type="submit" name="submit" value="Register">
                    <input type="hidden" name="action" value="sendregister"><br>
            </form>

  
    </main>

    <footer>
       <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/footer.php"); ?>
       </footer>
  </div>
</body>

</html>