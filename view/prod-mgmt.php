<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="CIT336">
  <meta name="keywords" content="PHP, CIT336, ACME">
  <meta name="author" content="Mabel">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>New Category</title>
  <link rel="stylesheet" type="text/css" href="/acme/css/style.css">
</head>

<body>
  <div id="content">
    <header>
      <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/header.php"); ?>
       </header>

    <nav>
    <?php echo $navList; ?>
    </nav>

    <main>
    <h1>Product Management</h1>
                <?php
                if (isset($message)) {
                    echo $message;
                }
                ?>
                <ul>
                <li><a href="/acme/products/index.php?action=formcat">Add A new Category</a></li>
                <li><a href="/acme/products/index.php?action=formprod">Add a NEW PRODUCTS</a></li>
                </ul>
    </main>

    <footer>
       <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/footer.php"); ?>
       </footer>
  </div>
</body>

</html>