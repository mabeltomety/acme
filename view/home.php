<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="description" content="CIT336">
  <meta name="keywords" content="PHP, CIT336, ACME">
  <meta name="author" content="Mabel">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ACME WEBSITE</title>
  <link rel="stylesheet" type="text/css" href="/acme/css/style2.css">
</head>

<body>
  <div id="content">
    <header>
      <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/header.php"); ?>
    </header>

    <nav>
      <?php //include($_SERVER['DOCUMENT_ROOT']."/Acme/common/nav.php"); ?>
      <?php echo $navList; ?>
    </nav>

    <main>
      <h1> Welcome to Acme!</h1>
      <section class="banner">
        <img class="banner-img" src="/acme/images/site/rocketfeature.jpg" alt="banner">
          <ul class="banner-ul">
            <li>
              <h2>Acme Rocket</h2>
            </li>
            <li>Quick lighting fuse</li>
            <li>NHTSA approved seat belts</li>
            <li>Mobile launch stand included</li>
            <li><a href="/acme/cart/"><img id="actionbtn" alt="Add to cart button" src="/acme/images/site/iwantit.gif"></a></li>
          </ul>
</section>

      <div id="bottom">

        <div class="gridcontainer">

          <p class="item1">Featured Recipes</p>
          <div class="item2">
            <figure>
              <img src="/acme/images/recipes/bbqsand.jpg" alt="bbq"/>
            </figure>

            <a href="#">BBQSAND</a>
          </div>
          <div class="item3">
            <figure>
              <img src="/acme/images/recipes/potpie.jpg" alt="potpie"/>
            </figure>

            <a href="#">POT PIE</a>

          </div>
          <div class="item4">
            <figure>
              <img src="/acme/images/recipes/soup.jpg" alt="soup" />
            </figure>

            <a href="#">SOUP</a>
          </div>
          <div class="item5">
            <figure>
              <img src="/acme/images/recipes/taco.jpg" alt="taco" />
            </figure>

            <a href="#">TACO</a>
          </div>
        </div>


        <div id="reviews">
          <p>Acme Rocket Review</p>
          <ul>
            <li>"I don't know how I ever caught roadrunners before this." (4/5)</li>
            <li>"That thing was fast!" (4/5)</li>
            <li>"Talk about fast delivery." (5/5)</li>
            <li>"I didn't even have to pull the meat apart." (4.5/5)</li>
            <li>"I'm on my thirtieth one. I love these things!" (5/5)</li>
          </ul>
        </div>

      </div>

    
    </main>

    <footer>
      <?php include($_SERVER['DOCUMENT_ROOT']."/Acme/common/footer.php"); ?>
    </footer>
  </div>
</body>

</html>