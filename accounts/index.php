<?php

// ACCOUNTS CONTROLLERS

// Get the database connection file
require_once '../library/connections.php';
// Get the acme model for use as needed
require_once '../model/acme-model.php';
require_once '../model/accounts-model.php';
// Get the functions library
require_once '../library/functions.php';
// Get the array of categories
$categories = getCategories();



// Build a navigation bar using the $categories array
// $navList = '<ul>';
// $navList .= "<li><a href='/acme/index.php' title='View the Acme home page'>Home</a></li>";
// foreach ($categories as $category) {
//  $navList .= "<li><a href='/acme/index.php?action=".urlencode($category['categoryName'])."' title='View our $category[categoryName] product line'>$category[categoryName]</a></li>";
// }
// $navList .= '</ul>';

$navList =buildNav($categories);

$action = filter_input(INPUT_POST, 'action');
 if ($action == NULL){
  $action = filter_input(INPUT_GET, 'action');
 }

 switch ($action) {
    case 'login':
      include '../view/login.php';
    break;

    case 'Login':
    
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $clientEmail = checkEmail($email);
    $passwords = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $checkPassword = checkPassword($passwords);

    // Check for missing data
    if (empty($firstname) || empty($lastname) || empty($clientEmail) || empty($checkPassword)) {
        $message = '<p>Please provide information for all empty form fields.</p>';
        include '../view/registration.php';
        exit;
    }
    break;
    
      case 'registerform':
        //echo ' inside the case';
        //exit;
        include '../view/registration.php';
        break;

    case 'sendregister':
        $firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
        $lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $clientEmail = checkEmail($email);
        $passwords = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
        $checkPassword = checkPassword($passwords);

        // Check for missing data
        if (empty($firstname) || empty($lastname) || empty($clientEmail) || empty($checkPassword)) {
            $message = '<p>Please provide information for all empty form fields.</p>';
            include '../view/registration.php';
            exit;
        }
        // Send the data to the model
        // Hash the checked password
$hashedPassword = password_hash($clientPassword, PASSWORD_DEFAULT);

// Send the data to the model
$regOutcome = regClient($clientFirstname, $clientLastname, $clientEmail, $hashedPassword);

        //var_dump($regOutcome);
        //exit;
        // Check and report the result
        if ($regOutcome === 1) {
            $message = "<p>Thanks for registering $firstname. Please use your email and password to login.</p>";
            include '../view/login.php';
            exit;
        } else {
            $message = "<p>Sorry $firstname, but the registration failed. Please try again.</p>";
            include '../view/registration.php';
            exit;
        }
        break;


    
    default:
   
   }
?>