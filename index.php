<?php

// ACME CONTROLLERS

// Get the database connection file
require_once 'library/connections.php';
// Get the acme model for use as needed
require_once 'model/acme-model.php';

// Get the functions library
require_once 'library/functions.php';
// Get the array of categories
$categories = getCategories();
$navList = buildNav($categories);
// Build a navigation bar using the $categories array
// $navList = '<ul>';
// $navList .= "<li><a href='/acme/index.php' title='View the Acme home page'>Home</a></li>";
// foreach ($categories as $category) {
//  $navList .= "<li><a href='/acme/index.php?action=".urlencode($category['categoryName'])."' title='View our $category[categoryName] product line'>$category[categoryName]</a></li>";
// }
// $navList .= '</ul>';

$action = filter_input(INPUT_POST, 'action');
 if ($action == NULL){
  $action = filter_input(INPUT_GET, 'action');
  if ($action == NULL){
    $action = 'home';
  }
 }

 switch ($action){
 case 'home':
    include 'view/home.php';
  break;
 
 default:
  include 'view/home.php';
}
?>