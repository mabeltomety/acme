<?php

//Products Controller
require_once '../library/connections.php';
require_once '../model/acme-model.php';

// Get the products model
require_once '../model/products-model.php';

// Get the functions library
require_once '../library/functions.php';
// Get the array of categories
// $categories = getCategories();
// $navlist = '<ul>';
// $navlist .= "<li><a href='.' title='View the Acme Homepage'>Home</a></li>";
// foreach ($categories as $category) {
//     $navlist .= "<li><a href='.?action=$category[categoryName]' title='View our $category[categoryName] product line'>$category[categoryName]</a></li>";
// }
// $navlist .= '</ul>';

$navList = buildNav($categories);

//Get the value from the action key - value pair
$action = filter_input(INPUT_POST, 'action');
 if ($action == NULL){
  $action = filter_input(INPUT_GET, 'action');
  if ($action == NULL){
    $action = 'prod-mgmt';
  }
 }


switch ($action) {
    case 'prod-mgmt':
        include '../view/prod-mgmt.php';
        break;
    case 'formcat':
            include '../view/new-cat.php';
            break;
    case 'formprod':
            include '../view/new-prod.php';
            break;

    case 'newProduct':
        // get data from (inventory table) form
        $invName = filter_input(INPUT_POST, 'invName', FILTER_SANITIZE_STRING);
        $invDescription = filter_input(INPUT_POST, 'invDescription', FILTER_SANITIZE_STRING);
        $invImage = filter_input(INPUT_POST, 'invImage', FILTER_SANITIZE_STRING);
        $invThumbnail = filter_input(INPUT_POST, 'invThumbnail', FILTER_SANITIZE_STRING);
        $invPrice = filter_input(INPUT_POST, 'invPrice', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $invStock = filter_input(INPUT_POST, 'invStock', FILTER_SANITIZE_NUMBER_INT);
        $invSize = filter_input(INPUT_POST, 'invSize', FILTER_SANITIZE_NUMBER_INT);
        $invWeight = filter_input(INPUT_POST, 'invWeight', FILTER_SANITIZE_NUMBER_INT);
        $invLocation = filter_input(INPUT_POST, 'invLocation', FILTER_SANITIZE_STRING);
        $categoryId = filter_input(INPUT_POST, 'categoryId', FILTER_SANITIZE_NUMBER_INT);
        $invVendor = filter_input(INPUT_POST, 'invVendor', FILTER_SANITIZE_STRING);
        $invStyle = filter_input(INPUT_POST, 'invStyle', FILTER_SANITIZE_STRING);

        // Validate the inputs
        //if empty
        if (empty($invName) || empty($invDescription) || empty($invImage) || empty($invPrice) || empty($invStock) || empty($categoryId) || empty($invVendor)) {
            $message = '<p class="bad">Please provide information for all empty form fields.</p>';
            echo $categoryId;
            include '../view/new-prod.php';
            exit;
        }

//var_dump($invName, $invDescription, $invImage, $invThumbnail, $invPrice, $invStock, $invSize, $invWeight, $invLocation, $categoryId, $invVendor, $invStyle);
//exit;
        $regOutcome = regProducts($invName, $invDescription, $invImage, $invThumbnail, $invPrice, $invStock, $invSize, $invWeight, $invLocation, $categoryId, $invVendor, $invStyle);

        // Check and report the result
        if ($regOutcome === 1) {
            $message = "<p class='good'>Thanks for registering $invName as a new product.</p>";
            include '../view/new-prod.php';
            exit;
        } else {
            $message = "<p class='bad'>Sorry $invName, but the registration for the product failed. Please try again.</p>";
            include '../view/new-prod.php';
            exit;
        }

        break;


    case 'newCategory':
        // get data from form
        $categoryName = filter_input(INPUT_POST, 'categoryName', FILTER_SANITIZE_STRING);

        // Validate the inputs
        //if empty
        if (empty($categoryName)) {
            $message = '<p class="bad"> * Please provide information for all empty form fields.</p>';
            include '../view/new-cat.php';
            exit;
        }

        $regOutcome = regCategories($categoryName);

        // Check and report the result
        if ($regOutcome === 1) {
            header('Location: /acme/products/index.php?action=prod-mgmt');
            exit;
        } else {
            $message = "<p class='bad'>Sorry $categoryName, but the registration for the category failed. Please try again.</p>";
            include '../view/new-cat.php';
            exit;
        }
        break;
}