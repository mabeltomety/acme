//INSERT

INSERT INTO `clients`(`clientFirstname`, `clientLastname`, `clientEmail`, `clientPassword`, `comments`)
VALUES ("Tony", "Stark", "tony@starkent.com", "Iam1ronM@n", "I am the real Ironman");

//UPDATE

UPDATE `clients` SET `clientLevel`= 3
WHERE clientFirstname = "Tony";

//UPDATE AND SELECT

UPDATE `inventory` 
SET invName = REPLACE(invName, "Nylon", "Climbing"),
invDescription = REPLACE(invDescription, "nylon", "climbing");

//INNER JOIN

SELECT inventory.invName, categories.categoryName
FROM inventory INNER JOIN categories
ON inventory.categoryId = categories.categoryId
WHERE categories.categoryName = "Misc";

//DELETE

DELETE FROM `inventory` WHERE invName="Koenigsegg CCX car";